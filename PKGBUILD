# U-Boot: Pinebook, based on Pine64 PKGBUILD
# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor: Kevin Mihelich <kevin@archlinuxarm.org>
# Contributor: Dragan Simic <dsimic@buserror.io>

pkgname=uboot-pinebook
pkgver=2022.10
pkgrel=2
_tfaver=2.8
_scpver=0.5
pkgdesc="U-Boot for Pine64 Pinebook with CRUST support"
arch=('aarch64')
url='http://www.denx.de/wiki/U-Boot/WebHome'
license=('GPL')
makedepends=('bc' 'python' 'python-setuptools' 'swig' 'dtc' 'arm-none-eabi-gcc' 'or1k-elf-gcc' 'or1k-elf-binutils' 'bison' 'flex')
provides=('uboot')
conflicts=('uboot')
install=${pkgname}.install
source=("ftp://ftp.denx.de/pub/u-boot/u-boot-${pkgver/rc/-rc}.tar.bz2"
        "https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/snapshot/trusted-firmware-a-${_tfaver}.tar.gz"
        "crust-${_scpver}.tar.gz::https://github.com/crust-firmware/crust/archive/refs/tags/v${_scpver}.tar.gz")
sha256sums=('50b4482a505bc281ba8470c399a3c26e145e29b23500bc35c50debd7fa46bdf8'
            'df4e0f3803479df0ea4cbf3330b59731bc2efc2112c951f9adb3685229163af9'
            '8b23b2649bbd19dfb84ae00b2419539b8236c6ae9a380beff7dffafd3f41f31b')

build() {
  # Avoid build warnings by editing a .config option in place instead of
  # appending an option to .config, if an option is already present
  update_config() {
    if ! grep -q "^$1=$2$" .config; then
      if grep -q "^# $1 is not set$" .config; then
        sed -i -e "s/^# $1 is not set$/$1=$2/g" .config
      elif grep -q "^$1=" .config; then
        sed -i -e "s/^$1=.*/$1=$2/g" .config
      else
        echo "$1=$2" >> .config
      fi
    fi
  }

  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS

  cd crust-${_scpver}

  echo -e "\nBuilding CRUST for Pine64 Pinebook...\n"
  make CROSS_COMPILE=or1k-elf- pinebook_defconfig
  make CROSS_COMPILE=or1k-elf- build/scp/scp.bin
  cp build/scp/scp.bin ../u-boot-${pkgver/rc/-rc}

  cd ../trusted-firmware-a-${_tfaver}

  echo -e "\nBuilding TF-A for Pine64 Pinebook...\n"
  make PLAT=sun50i_a64 SUNXI_AMEND_DTB=1 bl31
  cp build/sun50i_a64/release/bl31.bin ../u-boot-${pkgver/rc/-rc}

  cd ../u-boot-${pkgver/rc/-rc}

  echo -e "\nBuilding U-Boot for Pine64 Pinebook...\n"
  make pinebook_defconfig

  update_config 'CONFIG_IDENT_STRING' '" Manjaro Linux ARM"'

  make EXTRAVERSION=-${pkgrel}
  cp u-boot-sunxi-with-spl.bin u-boot-sunxi-with-spl-pinebook.bin
}

package() {
  cd u-boot-${pkgver/rc/-rc}

  mkdir -p "${pkgdir}/boot/extlinux"
  install -D -m 0644 u-boot-sunxi-with-spl-pinebook.bin -t "${pkgdir}/boot"
}
